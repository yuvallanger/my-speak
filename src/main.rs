#[macro_use]
extern crate failure;

use confy;
use directories;
use serde::Deserialize;
use serde::Serialize;
use std::io::Read;
use std::process::Command;
use structopt::StructOpt;

type Result<T> = core::result::Result<T, MySpeakError>;

#[derive(Debug, Default, Serialize, Deserialize)]
struct Configuration {
    words_per_minute: u32,
}

#[derive(Debug, StructOpt)]
struct Cli {
    #[structopt(long = "set-words-per-minute")]
    set_words_per_minute: Option<u32>,

    #[structopt(long = "kill")]
    kill: bool,

    #[structopt(long = "wpm")]
    words_per_minute: Option<u32>,
}

fn get_line() -> Result<String> {
    let mut xsel_output_string: String = String::new();

    {
        let xsel_child = Command::new("xsel")
            .stdout(std::process::Stdio::piped())
            .spawn()
            .map_err(MySpeakError::XSelSpawn)?;

        let mut xsel_stdout = xsel_child.stdout.ok_or(MySpeakError::NoXSelStdout)?;

        xsel_stdout
            .read_to_string(&mut xsel_output_string)
            .map_err(MySpeakError::ReadXSelString)?;
    };

    {
        let split = xsel_output_string.split_whitespace();
        let vec: Vec<_> = split.collect();
        Ok(vec.join(" "))
    }
}

fn speak(speak_string: &str, wpm: u32) -> Result<()> {
    let base_dirs = directories::BaseDirs::new().unwrap();
    let config_dir_path = base_dirs.config_dir();

    let my_speak_config_path = config_dir_path.join("my-speak");

    let lock_file_path = my_speak_config_path.join("lock");
    let pid_file_path = my_speak_config_path.join("pid");

    // XXX: We use the "which" command to get the complete path of the "espeak-ng"
    // executable because "daemonize" only takes complete paths.
    let which_espeak_ng_child = Command::new("which")
        .arg("espeak-ng")
        .stdout(std::process::Stdio::piped())
        .spawn()
        .unwrap();

    let mut espeak_ng_path_string = String::new();
    which_espeak_ng_child
        .stdout
        .unwrap()
        .read_to_string(&mut espeak_ng_path_string)
        .unwrap();
    let espeak_ng_path_string = espeak_ng_path_string.trim_end();

    let mut x = Command::new("daemonize")
        .arg("-v")
        .args(&["-l", lock_file_path.to_str().unwrap()])
        .args(&["-p", pid_file_path.to_str().unwrap()])
        .arg(espeak_ng_path_string)
        .args(&["-s", format!("{}", wpm).as_str()])
        .args(&["--", speak_string])
        .spawn()
        .map_err(MySpeakError::EspeakNGSpawn)?;

    x.wait().map_err(MySpeakError::EspeakNGWait)?;

    Ok(())
}

#[derive(Debug, Fail)]
enum MySpeakError {
    #[fail(display = "Unable to load configuration: {}", _0)]
    LoadConfiguration(confy::ConfyError),

    #[fail(display = "Unable to spawn xsel process: {}", _0)]
    XSelSpawn(std::io::Error),

    #[fail(display = "No xsel stdout.")]
    NoXSelStdout,

    #[fail(display = "Unable to read xsel output into a string: {}", _0)]
    ReadXSelString(std::io::Error),

    #[fail(display = "Unable to spawn espeak-ng process: {}", _0)]
    EspeakNGSpawn(std::io::Error),

    #[fail(display = "Unable to wait for espeak-ng process: {}", _0)]
    EspeakNGWait(std::io::Error),

    #[fail(display = "Unable to run kill: {}", _0)]
    UnableToRunKill(nix::Error),

    #[fail(display = "Unable to parse pid: {}", _0)]
    UnableToParsePid(std::num::ParseIntError),

    #[fail(display = "Failed to store configuration: {}", _0)]
    FailedToStoreConfiguration(confy::ConfyError),
}

fn kill_speak() -> Result<()> {
    let base_dirs = directories::BaseDirs::new().unwrap();
    let config_dir_path = base_dirs.config_dir();
    let my_speak_config_dir_pathbuf = config_dir_path.join("my-speak");
    let pid_file_pathbuf = my_speak_config_dir_pathbuf.join("pid");

    let pid = std::fs::read_to_string(pid_file_pathbuf).unwrap();

    let pid_t = pid
        .trim()
        .parse::<i32>()
        .map_err(MySpeakError::UnableToParsePid)?;

    if pid_t > 0 {
        let nix_pid = nix::unistd::Pid::from_raw(pid_t);
        nix::sys::signal::kill(nix_pid, nix::sys::signal::SIGKILL)
            .map_err(MySpeakError::UnableToRunKill)?;
    }

    Ok(())
}

fn main() -> Result<()> {
    let configuration: Configuration =
        confy::load("my-speak").map_err(MySpeakError::LoadConfiguration)?;
    let cli_arguments: Cli = Cli::from_args();

    if cli_arguments.kill {
        return kill_speak();
    }

    if let Some(set_words_per_minute) = cli_arguments.set_words_per_minute {
        let new_configuration = Configuration {
            words_per_minute: set_words_per_minute,
        };

        confy::store("my-speak", new_configuration)
            .map_err(MySpeakError::FailedToStoreConfiguration)?;

        return Ok(());
    }

    let mut words_per_minute_value = configuration.words_per_minute;

    if cli_arguments.words_per_minute.is_some() {
        words_per_minute_value = cli_arguments.words_per_minute.unwrap();
    };

    let our_line = get_line()?;

    speak(&our_line, words_per_minute_value)?;

    Ok(())
}
